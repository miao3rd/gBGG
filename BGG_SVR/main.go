package main

import (
	"io"
	"os"
	"time"

	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	irisRecover "github.com/kataras/iris/middleware/recover"

	"tetris/comm"
	"tetris/db"
	"tetris/net"
)

var g_app = iris.New()
var g_config = iris.TOML("./cfg/iris.tml")

func main() {
	defer comm.Tools_Dump()

	comm.App(g_app)

	logFile := comm.Tools_Get_LogFile()
	defer logFile.Close()
	g_app.Logger().SetOutput(io.MultiWriter(logFile, os.Stdout))
	g_app.Logger().SetLevel("warn")

	g_app.Use(irisRecover.New())
	g_app.Use(logger.New())

	// web ---
	g_app.RegisterView(iris.HTML("./templates", ".html").Reload(true))
	g_app.StaticWeb("/", "./static")
	g_app.Favicon("./static/favicon.ico")
	g_app.Use(iris.Cache304(time.Minute * 60))

	// 需要跨域页面
	crs := func(ctx iris.Context) {
		ctx.Header("Access-Control-Allow-Origin", "*")
		ctx.Header("Access-Control-Allow-Credentials", "true")
		ctx.Header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin,Content-Type")
		ctx.Next()
	}
	v1 := g_app.Party("/", crs).AllowMethods(iris.MethodOptions)
	{
		v1.Get("/game", GetGame)
	}

	// websocket
	ws := net.WebSocket_Init()
	g_app.Get("/ws", ws.Handler())
	// ---

	// mysql ---
	err := db.DB_Init(Get_Cfg_Str("DB"))
	if err != nil {
		panic(err)
	}
	defer db.DB_Close()
	// ---

	comm.Tools_Create_Stop_File()

	// a whole new world ---
	go Wolrd_Create(Get_Cfg_Int("FPS"))
	// ---

	g_app.Run(iris.Addr(Get_Cfg_Str("Port")), iris.WithConfiguration(g_config))
}

func Get_Cfg_Str(key string) string {
	config_other := g_config.GetOther()
	value, ok := config_other[key].(string)
	if !ok {
		g_app.Logger().Error("Get_Cfg_Str err! key", key)
		return ""
	}
	return value
}

func Get_Cfg_Int(key string) int {
	config_other := g_config.GetOther()
	value, ok := config_other[key].(int64)
	if !ok {
		g_app.Logger().Errorf("Get_Cfg_Int err! key=%s cfg=%v", key, config_other)
		return -1
	}
	return int(value)
}
